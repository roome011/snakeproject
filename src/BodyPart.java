import java.awt.Color;
import java.awt.Graphics;

public class BodyPart {
	
	private int x, y;
	
	public BodyPart(int xCoor, int yCoor) {
		x = xCoor;
		y = yCoor;
	}

	/**
	 * Draws this body part.
	 * 
	 * The corresponding tile coordinates are defined by the x * tileWidth and the y * tileHeight.
	 * This way the game view becomes an 80 by 80 grid instead of 800 by 800 pixels.
	 * 
	 * @param graphics
	 */
	public void draw(Graphics graphics) {
		graphics.setColor(Color.CYAN);
		int size = GameView.TILESIZE;
		graphics.fillRect(x * size, y * size, size, size);
	}
	
	public void drawRed(Graphics graphics) {
		graphics.setColor(Color.RED);
		int size = GameView.TILESIZE;
		graphics.fillRect(x * size, y * size, size, size);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
