import java.awt.Graphics;
import java.util.ArrayList;

public class Snake{
	
	private ArrayList<BodyPart> bodyParts;
	
	private int x = 10, y = 10, snakeLength = 5;
	private Direction direction;
	
	public Snake() {
		bodyParts = new ArrayList<BodyPart>();
		addBodyPart(false);
		direction = Direction.RIGHT;
	}
	
	/**
	 * Adds a bodyPart to the snake at the position of the head.
	 * 
	 * @param isCollided tells if there was a collision with a pickup. If yes the snakeLength is increased.
	 */
	public void addBodyPart(Boolean isCollided) {
		bodyParts.add(new BodyPart(x, y));
		if(isCollided) snakeLength++;
	}
	
	/**
	 * Updates the snake's position every tick.
	 * 
	 * With Direction the snake decides which way it should move. It spawns a new BodyPart there.
	 * When the array of bodyParts reaches the amount of snakeLength, it starts removing a piece at the end of the tail.
	 */
	public void updatePosition() {
		if(direction == Direction.RIGHT) x++;
		if(direction == Direction.LEFT) x--;
		if(direction == Direction.UP) y--;
		if(direction == Direction.DOWN) y++;
		
		addBodyPart(false);
		
		if(bodyParts.size() == snakeLength) {
			bodyParts.remove(0);
		}
	}
	/**
	 * Checks if there is collision.
	 * 
	 * First check if there is collision with the pickup.
	 * Check if there is collision with self. It compares the position of the head with the position of the rest of the body.
	 * Check if there is collision with borders.
	 * 
	 * @param pickup describes the pickup that the snake can eat.
	 */
	public void checkCollision(Pickup pickup){
		BodyPart head = bodyParts.get(bodyParts.size() - 1);
		
		if(x == pickup.getX() && y == pickup.getY()) {
			addBodyPart(true);
			pickup.setRandomPosition();
			return;
		}
		
		for(int i = 0 ; i < bodyParts.size() - 1 ; i++) {
			BodyPart currentPart = bodyParts.get(i);
			if(head.getX() == currentPart.getX() && head.getY() == currentPart.getY()) {
				GameView.stop(true, snakeLength);
			}
		}
		
		if(head.getX() < 0 || head.getX() > GameView.BOARDSIZE_X || head.getY() < 0 || head.getY() > GameView.BOARDSIZE_Y) {
			GameView.stop(true, snakeLength);
		}
	}
	
	public void paint(Graphics graphics) {
		for(int i = 0; i < bodyParts.size(); i++) {
			bodyParts.get(i).draw(graphics);
		}
	}
	
	public enum Direction {
		RIGHT,
		LEFT,
		UP,
		DOWN
	}
	
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
	
	public Direction getDirection() {
		return direction;
	}
}
