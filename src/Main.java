import javax.swing.JFrame;

public class Main {
	
	/**
	 * Creates the window for the game.
	 */
	public Main() {
		JFrame frame = new JFrame();
		GameView gameView = new GameView();
		
		frame.add(gameView);
		frame.pack();
		
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Snake");
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}

	/**
	 * Starts the game.
	 * @param args
	 */
	public static void main(String[] args) {
		new Main();
	}
}
