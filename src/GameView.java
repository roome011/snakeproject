import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class GameView extends JPanel implements Runnable, KeyListener{
	
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 500, HEIGHT = 500;
	public static final int TILESIZE = 10; //TODO: make tileSize dependent on width and height;
	public static final int BOARDSIZE_X = (WIDTH / TILESIZE) - 1, BOARDSIZE_Y = (HEIGHT / TILESIZE) - 1;
	private static final int TICKTIME = 2000000;
	
	private static Thread thread;
	private Snake snake;
	private Pickup pickup;
	
	private static boolean running;
	private static boolean isGameOver;
	private static int snakeLength;
	private int ticks = 0;
	
	public GameView() {
		setFocusable(true);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		
		start();
	}
	
	public void start() {
		snake = new Snake();
		pickup = new Pickup();
		thread = new Thread(this);
		thread.start();
		running = true;	
		isGameOver = false;
	}
	
	/**
	 * Stops the moving of the snake.
	 * 
	 * @param isGameOverSnake tells the game if it has to display the game over text.
	 * @param snakeLengthFinal tells the game what the final snakeLength is.
	 */
	public static void stop(Boolean isGameOverSnake, int snakeLengthFinal) {
		isGameOver = isGameOverSnake;
		snakeLength = snakeLengthFinal;
		running = false;	
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Automatically called by repaint(), assume the graphics are cleared before calling paint.
	 */
	public void paint(Graphics graphics) {
		graphics.clearRect(0, 0, WIDTH, HEIGHT);
		graphics.setColor(Color.DARK_GRAY);
		graphics.fillRect(0, 0, WIDTH, HEIGHT);
		
		snake.paint(graphics);
		pickup.paint(graphics);
		
		if(isGameOver) {
			graphics.setColor(Color.WHITE);
			graphics.setFont(new Font("arial", Font.BOLD, 50));
			graphics.drawString("Game Over", WIDTH / 4, HEIGHT / 2);
			graphics.setFont(new Font("arial", Font.BOLD, 15));
			graphics.drawString("Your final snakelength was " + snakeLength + " press <space> to restart", HEIGHT / 8, HEIGHT - (HEIGHT / 3));
		}
	}
	/**
	 * Run starts when the Thread is ready.
	 * 
	 * Because snake.update() and snake.checkCollision(pickup) shouldn't be called every tick, wait for a certain amount of ticks (TICKTIME).
	 */
	@Override
	public void run() {
		while(running) {
			ticks++;
			if(ticks > TICKTIME) {
				snake.updatePosition();
				snake.checkCollision(pickup);
				ticks = 0;
			}
			repaint();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub		
	}

	/**
	 * Converts user input to Snake.Direction and restart the game.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if((key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) && snake.getDirection() != Snake.Direction.LEFT ) {
			snake.setDirection(Snake.Direction.RIGHT);
		}
		if((key == KeyEvent.VK_LEFT  || key == KeyEvent.VK_A) && snake.getDirection() != Snake.Direction.RIGHT ) {
			snake.setDirection(Snake.Direction.LEFT);
		}
		if((key == KeyEvent.VK_UP || key == KeyEvent.VK_W) && snake.getDirection() != Snake.Direction.DOWN ) {
			snake.setDirection(Snake.Direction.UP);
		}
		if((key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) && snake.getDirection() != Snake.Direction.UP ) {
			snake.setDirection(Snake.Direction.DOWN);
		}
		if(key == KeyEvent.VK_SPACE && isGameOver) {
			start();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}
}
