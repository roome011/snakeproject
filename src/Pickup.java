import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Pickup {
	
	private int x, y;
	
	private Random randomNumberGenerator;
	
	public Pickup() {
		randomNumberGenerator = new Random();
		setRandomPosition();
	}
	
	public void setRandomPosition() {
		x = randomNumberGenerator.nextInt(GameView.BOARDSIZE_X);
		y = randomNumberGenerator.nextInt(GameView.BOARDSIZE_Y);
	}
	
	/**
	 * Draws this pickup.
	 * 
	 * The corresponding tile coordinates are defined by the x * tileWidth and the y * tileHeight.
	 * This way the game view becomes an 80 by 80 grid instead of 800 by 800 pixels.
	 * 
	 * @param graphics
	 */
	
	public void draw (Graphics graphics) {
		graphics.setColor(Color.PINK);
		int size = GameView.TILESIZE;
		graphics.fillRect(x * size, y * size, size, size);
	}
	
	public void paint(Graphics graphics) {
		draw(graphics);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
